dokumentacja:
git:
https://bitbucket.org/s13037/jaz_5

Na początku każdego adresu dodaj: http://localhost:8080/samplerest/rest/
Metoda	Adres url
GET	movies/			Pobiera listę filmów
POST	movies/			Dodaje nowy film
GET	movies/{id}		Pobranie filmu o podanym id
DELETE	movies/{id}		Usunięcie filmu o podanym id
GET	movies/{id}/grade	Pobranie średniej oceny filmu o podanym id
GET	movies/{id}/grades	Pobranie ocen do filmu o podanym id
POST	movies/{id}/grades	Dodanie oceny do filmu o podanym id
	

