package domain.services;

import java.util.ArrayList;
import java.util.Random;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;

import domain.Grade;

public class GradeService {
//	Map<Integer, List<Grade>> grades = new HashMap<Integer, ArrayList<Grade>>();
	static ListMultimap<Integer, Grade> grades = ArrayListMultimap.create();
	static int currentId = 0;
	
	public GradeService(){
		if (grades.size() == 0)
			this.addSampleContent();
	}
	
	public void addSampleContent(){
		Random random = new Random();
		for(int i=0; i<18; i++){
			Grade g1 = new Grade();
			g1.setNumber(random.nextInt(9));
			g1.setUserName("anon");
			if (i%10 == 0)
				g1.setUserName("marcin");
			else if (i%8 == 0)
				g1.setUserName("artur");
			this.add(1 + i%3, g1);
		}
	}
	
	public List<Grade> getGradesForMovie(int movieId){
		return grades.get(movieId);
	}
	
	public void add(int movieId, Grade grade){
		grade.setId(++currentId);
		grades.put(movieId, grade);
	}
	
	public float getAvarageGradeForMovie(int movieId){
		int grades = 0;
		int sum = 0;
		for (Grade grade : GradeService.grades.get(movieId)){
			sum += grade.getNumber();
			grades++;
		}
		return (float)sum/(float)grades;
	}
	
}
