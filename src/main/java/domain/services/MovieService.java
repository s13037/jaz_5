package domain.services;

import java.util.*;

import domain.Movie;

public class MovieService {
	private static List<Movie> movies = new ArrayList<Movie>();
	private static int currentId = 0;
	
	public MovieService(){
		if(currentId == 0)
			this.addSampleContent();
	}
	
	public void addSampleContent(){
		Movie pulp = new Movie();
		pulp.setYear(1994);
		pulp.setTitle("Pulp Fiction");
		pulp.setDirector("Quentin Tarantino");
		Movie fight = new Movie();
		fight.setYear(1999);
		fight.setTitle("Fight Club");
		fight.setDirector("David Fincher");
		Movie psy = new Movie();
		psy.setYear(1992);
		psy.setTitle("Psy");
		psy.setDirector("Pasikowski");
		this.add(pulp);
		this.add(fight);
		this.add(psy);
	}
	
	public List<Movie> getAll(){
		return movies;
	}
	
	public Movie get(int id){
		for (Movie movie : movies){
			if (movie.getId()==id)
				return movie;
		}
		return null;
	}
	
	public void add(Movie m){
		m.setId(++currentId);
		movies.add(m);
	}
	
	public void delete(Movie m){
		movies.remove(m);
	}

}
