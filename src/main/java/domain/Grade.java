package domain;

public class Grade {
	private int id = 0;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private int number = 0;
	private String userName = "";
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		if (number>10)
			number = 10;
		else if (number<0)
			number = 0;
		this.number = number;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	

}
