package rest;

import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Grade;
import domain.Movie;
import domain.services.*;

@Path ("/movies")
public class MovieResources {
	private MovieService moviesDB = new MovieService();
	private GradeService gradesDB = new GradeService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Movie> getAll(){
		return moviesDB.getAll();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Movie get(@PathParam("id") int id){
		return moviesDB.get(id);
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") int id){
		Movie m = moviesDB.get(id);
		if (m == null)
			return Response.status(404).build();
		moviesDB.delete(m);
		return Response.ok().build();
	}
		
	@GET
	@Path("/{id}/grade")
	@Produces(MediaType.TEXT_PLAIN)
	public float getGrade(@PathParam("id") int movieId){
		return gradesDB.getAvarageGradeForMovie(movieId);
	}


	@GET
	@Path("/{id}/grades")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Grade> getGrades(@PathParam("id") int movieId){
		return gradesDB.getGradesForMovie(movieId);

	}

	@POST
	@Path("/{id}/grades")
	@Consumes (MediaType.APPLICATION_JSON)
	public Response addGrade(@PathParam("id") int movieId, Grade grade){
		gradesDB.add(movieId, grade);
		return Response.ok(grade.getId()).build();

	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(Movie movie){
		moviesDB.add(movie);
		return Response.ok(movie.getId()).build();
	}

}
